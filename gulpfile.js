'use strict';

var gulp        = require('gulp'),
    sass        = require('gulp-sass'),
    eslint      = require('gulp-eslint'),
    concat      = require('gulp-concat'),
    cached      = require('gulp-cached'),
    changed     = require('gulp-changed'),
    autoprefix  = require('gulp-autoprefixer'),
    gulpif      = require('gulp-if'),
    cached      = require('gulp-cached'),
    gutil       = require('gulp-util'),
    notify      = require('gulp-notify'),
    plumber     = require('gulp-plumber'),
    sourcemaps  = require('gulp-sourcemaps'),
    livereload  = require('gulp-livereload'),
    uglify      = require('gulp-uglify'),
    nano        = require('gulp-cssnano'),
    browserify  = require('browserify'),
    stringify   = require('stringify'),
    source      = require('vinyl-source-stream'),
    buffer      = require('vinyl-buffer'),
    del         = require('del'),
    argv        = require('yargs').argv;

/**
* Define static libs for js vendor bundle
*/
var libs = [
  "tabletop",
  "angular",
  "angular-ui-router",
  "flickity"
];

/**
 * Production flag
 * @usage `gulp --production`
 */
var production = !!argv.production;

/**
 * Error handling
 */
var onError = function (task) {
  return function (err) {

    notify.onError({
      message: task + ' failed'
    })(err);

    gutil.log( gutil.colors.bgRed(task + ' error:'), gutil.colors.red(err) );
  };
};

/**
  * Setup source globs
  */
var dir = {
  source: 'src/',
  build:  'dist/'
};

var sources = {
  images:    [ dir.source + 'img/*' ],
  app:       [ dir.source + 'app/app.js' ],
  js:        [ dir.source + 'app/**/*.js'],
  fonts:     [ dir.source + 'fonts/**' ],
  html:      [ dir.source + '*.html' ],
  templates: [ dir.source + 'app/**/*.html' ],
  css:       [ dir.source + 'sass/main.scss' ],
  allCss:    [ dir.source + '**/*.scss' ]
};

/**
 * Default task - build and watch
 * @usage `gulp`
 */
gulp.task('default', ['build', 'watch']);

/**
 * Build task
 * @usage `gulp build`
 */
gulp.task('build', ['images', 'fonts', 'html', 'css', 'vendorjs', 'js']);

/**
 * Clean build folder
 * @usage `gulp clean`
 */
gulp.task('clean', function () {
  del(dir.build + '**');
});

/**
 * Copy images
 * @usage `gulp images`
 */
gulp.task('images', function () {

  return gulp.src(sources.images)
    .pipe(changed(dir.build + 'img'))
    .pipe(gulp.dest(dir.build + 'img'));
});

/**
 * Copy fonts
 * @usage `gulp fonts`
 */
gulp.task('fonts', function () {

  return gulp.src(sources.fonts)
    .pipe(changed(dir.build + 'fonts'))
    .pipe(gulp.dest(dir.build + 'fonts'));
});

/**
 * Process javascript
 * Lint with `jshint-stylish` reporter
 * @usage `gulp lint`
 */
gulp.task('lint', function () {

  return gulp.src(sources.js)
    .pipe(eslint())
    .pipe(eslint.format())
    // To have the process exit with an error code (1) on
    // lint error, return the stream and pipe to failAfterError last.
    .pipe(eslint.failAfterError())
    .on('error', onError);
});

/**
 * Create vendor bundle for static js libs
 * To avoid recompiling everything all the time
 * @see https://github.com/sogko/gulp-recipes/tree/master/browserify-separating-app-and-vendor-bundles
 * @usage `gulp vendorjs`
 */
gulp.task('vendorjs', function () {

  var b = browserify({
    debug: !production,
    transform: [stringify]
  });

  libs.forEach(function(lib) {
    b.require(lib);
  });

  return b.bundle()
    .on('error', onError('js'))
    .pipe(source('vendor.js'))
    .pipe(buffer())
    .pipe(gulpif(production, uglify()))
    .pipe(gulp.dest(dir.build + 'js'));
});

/**
 * Process javascript
 * Browserify everything, exclude libraries
 * @usage `gulp js`
 */
gulp.task('js', ['lint'], function () {

  var b = browserify({
    entries: sources.app,
    debug: true,
    transform: [stringify]
  });

  // let browserify know we will load libs from an external source
  // vendor.js in this case
  libs.forEach(function (lib) {
    b.external(lib);
  });

  return b.bundle()
    .on('error', onError('js'))
    .pipe(source('app.js'))
    .pipe(buffer())
    .pipe(sourcemaps.init({loadMaps:true}))
    .pipe(gulpif(production, uglify()))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest(dir.build + 'js'));
});

/**
 * Copy HTML files
 * @usage `gulp html`
 */
gulp.task('html', function () {
  return gulp.src(sources.html)
    .pipe(gulp.dest(dir.build));
});

/**
 * Process SASS
 * Generate source maps
 * Add vendor prefixes - autoprefix
 * Combines to one file `style.css`
 * @usage `gulp css`
 */
gulp.task('css', function () {

  return gulp.src(sources.css)
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefix())
    .pipe(concat('style.css'))
    .pipe(gulpif(production, nano()))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(dir.build + 'css'))
    .pipe(livereload());
});

/**
 * Watch me for the changes
 * @usage `gulp watch`
 */
gulp.task('watch', function () {

  livereload.listen();

  gulp.watch(sources.html, ['html']);
  gulp.watch(sources.images, ['images']);
  gulp.watch(sources.allCss, ['css']);
  gulp.watch(sources.js, ['js']);
  gulp.watch(sources.templates, ['js']);

  gulp.watch(dir.build + '**/*').on('change', livereload.changed);
});