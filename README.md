# Interactive - Nespresso
### Google Sheet - How to change
- Create a copy of the blank spreadsheet `https://docs.google.com/spreadsheets/d/1bBcFpGobB1UYJv6n5WWuOJQA6mBNYXXtW-2nFZlVBxU/edit#gid=0`
- Publish new spreadsheet to web `File > Publish to web`
- Copy link and paste into app.routes.js as `key` for Tabletop.js:-
```
  TabletopProvider.setTabletopOptions({
    key: 'https://docs.google.com/spreadsheets/d/10Bmwet9C7dIgB822lX7IUtFY3En4swv0Nw50I_q4CKY/pubhtml'
  });
```
# Dev Prerequisites
- [Node](https://nodejs.org/ "Node") v.4.1.1 installed  (to manage node versions use [NVM](https://github.com/creationix/nvm) )
- [NPM](https://www.npmjs.com/ "NPM Package Manager") installed
### Setup local dev environment
- Clone this repository and change dir to folder
- Run the following commands:
- `npm install`
- `gulp`
- Files are copied to `dist` folder
- For minified version, run `gulp --production`
### Notes on conversion to Require.js for boot.js
##### General
- The Angular app is currently compiled with browserify using gulp
- This needs to be refactored to use AMD modules to work with inline loading in boot.js
##### Steps to convert
- Remove browserify task and replace with require task, [example gulpfile](https://github.com/phated/requirejs-example-gulpfile/blob/master/gulpfile.js)
- Change module loader in each file to use Require.js
- Ensure all dependencies are loaded correctly using Require.js method
- eg. in app.js, replace all browserify methods such as `require('./shared/tracking/').name` with Require.js style naming of components and dependencies
- Setup config file to load necessary libraries
- Test with guardian interactive [boot.js](https://github.com/guardian/interactive-journey-2015/)
- As mentioned in in-line loading with boot.js, all assets including HTML need to by dynamically loaded
- This is mostly taken care of with templates, only exception is the index.html
##### Libraries to include
- Angular.js
- Tabletop.js
- Flickity.js
##### App Folder Structure
```
+-- app
|   +-- components
|   +-- shared
|   +-- app.js `Entry point for Angular app`
|   +-- app.routes.js `Config for Angular app`
+-- fonts
+-- sass
|   +-- _modules.scss `All component stylesheets are included here`
+-- index.html
```