var galleryDirective = require('./gallery-directive');

module.exports = angular
  .module('gl-app.lightbox', [])
  .directive('glLightbox', galleryDirective);
