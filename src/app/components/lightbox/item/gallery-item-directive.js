module.exports = ['$log', 'SHEETS', function ($log, SHEETS) {

  function handleImageError() {
    if (this.$emit) this.$emit('lightbox.image.error');
  }

  function handleImageLoad() {
    $log.debug('Gallery: Image loaded');
    if (this.$emit) this.$emit('lightbox.image.load');
  }

  return {
    scope: {
      image: '='
    },
    restrict: 'EA',
    replace: true,
    template: require('./template.html'),
    controller: ['$scope', function ($scope) {
      if ($scope.image && $scope.image.caption) {
        $scope.caption = $scope.image.caption;
      }
    }],
    link: function (scope, element, attrs) {

      // Create image element for loading purposes
      var img = new Image();
      img.onload = function () {
        var spinner = angular.element(element[0].querySelector('.gl-gallery-spinner-container'));
        spinner.css({
          display: 'none'
        });
        handleImageLoad.call(scope);
      };
      img.onerror = function () {
        handleImageError.call(scope);
      };
      element.append(img);

      // Listen for lightbox open before set image src
      scope.$on('lightbox.open', function () {
        if (!img.src) {
          if (scope.image && scope.image.src) {
            img.src = scope.image.src;

            element.css({
              "background-image": "url(" + scope.image.src + ")"
            });
          }
        }
      });

      // Capture clicks on image
      element.on('click', function (e) {
        e.stopPropagation();
      });
    }
  };
}];