var galleryItemDirective = require('./gallery-item-directive');
    
module.exports = angular
  .module('gl-app.lightbox.item', [])
  .directive('glGalleryItem', galleryItemDirective);  