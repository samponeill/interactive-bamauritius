var Flickity = require('flickity');

module.exports = ['$log', '$window', '$timeout', 'Colours', 'SHEETS', function ($log, $window, $timeout, Colours, SHEETS) {
  return {
    restrict: 'EA',
    replace: true,
    template: require('./template.html'),
    controller: ['$scope', function ($scope) {
      // Set up image list
      $scope.images = [];

      function isImage(k) {
        return k.match(/image/) && !k.match(/icon/);
      }

      function getImageId(k) {
        return k.match(/_([a-z0-9])+/)[0];
      }

      /**
       * Get image source in preference order:
       * Portrait, Mobile, Desktop
       */
      function getImageSrc(k, value, id) {
        var portraitKey, mobileKey, desktopKey;

        portraitKey = 'image' + id + '_portrait';
        mobileKey   = 'image' + id + '_mobile';
        desktopKey  = 'image' + id;

        if (value[portraitKey]) {
          return value[portraitKey];
        }
        if (value[mobileKey]) {
          return value[mobileKey];
        }
        if (value[desktopKey]) {
          return value[desktopKey];
        }

        return null;
      }

      function getCaption(k, value, id) {
        return value['caption' + id];
      }

      var foundIds = [];

      // Populate image list from sheet
      if ($scope.data && $scope.data[SHEETS.CONTENT]) {
        var all = $scope.data[SHEETS.CONTENT].all();
        // Loop through each sheet element
        angular.forEach(all, function (value) {
          // Find all images and push to images array
          Object.keys(value).forEach(function (k) {
            // If key contains 'image' and image is not empty
            // This is an image key
            if (isImage(k) && value[k]) {
              // Get id from image
              var id, uniqId;
              id = getImageId(k);
              // Unique id is image id + page id
              uniqId = id + '_' + value.id;
              // Has this image been set?
              if (foundIds.indexOf(uniqId) < 0) {
                if (getImageSrc(k, value, id)) {
                  // Store image
                  $scope.images.push({
                    src: getImageSrc(k, value, id),
                    caption: getCaption(k, value, id),
                    photoId: uniqId
                  });

                  // Store uniqId so we dont look again
                  foundIds.push(uniqId);
                }
              }
            }
          });
        }, $scope.images);
      }
    }],
    link: function (scope, element) {
      // For Flickity start index
      var photoIndex = 0;

      // Flickity
      var flick;
      function initFlickity() {
        flick = new Flickity('.gl-lightbox-wrapper', {
          initialIndex: photoIndex,
          cellSelector: '.gl-gallery-item',
          prevNextButtons: false,
          setGallerySize: false,
          resize: true,
          freeScroll: false,
          pageDots: false,
          selectedAttraction: 0.15,
          friction: 0.9
        });
      }
      function destroyFlickity() {
        if (flick) {
          flick.destroy();
        }
      }
      function setGalleryIndex(index) {
        if (flick) {
          flick.select(index, false, true);
        } else {
          $log.debug('Cannot set index: Flickity not yet initialised');
        }
      }

      function getPhotoIndexById(id) {
        var res = 0;
        // Loop through each sheet element
        angular.forEach(scope.images, function (value, key) {
          if (value.photoId === id) {
            res = key;
          }
        });
        return res;
      }

      var handlePressRight = function () {
        if (flick) {
          flick.next(true);
        } else {
          $log.debug('Cannot go right: No Flickity');
        }
      }.bind(this);

      var handlePressLeft = function () {
        if (flick) {
          flick.previous(true);
        } else {
          $log.debug('Cannot go left: No Flickity');
        }
      }.bind(this);

      function removeListeners() {
        angular.element($window.document).off('keydown');
        angular.element(element[0].querySelector('.btn-right')).off('click', handlePressRight);
        angular.element(element[0].querySelector('.btn-left')).off('click', handlePressLeft);
      }

      var closeLightbox = function () {
        element.removeClass('open');
        removeListeners();
      }.bind(this);

      function handleKeyDown(e) {
        var ESC = 27;

        if (e.keyCode === ESC) {
          closeLightbox();
        }
      }

      function addListeners() {
        // Add key listener (esc)
        angular.element($window.document).on('keydown', handleKeyDown);
        angular.element(element[0].querySelector('.btn-right')).on('click', handlePressRight);
        angular.element(element[0].querySelector('.btn-left')).on('click', handlePressLeft);
      }

      scope.onCloseClick = function () {
        closeLightbox();
      };

      // Open lightbox
      scope.$on('lightbox.open', function (e, photoId) {
        element.addClass('open');
        photoIndex = getPhotoIndexById(photoId);
        setGalleryIndex(photoIndex);
        addListeners();
      });

      // Listen for orientation / resize and close if open
      scope.$on('app.resize', function () {
        if (element.hasClass('open')) {
          closeLightbox();
        }
      });

      scope.$on('$destroy', function () {
        destroyFlickity();
      });

      // Set colour of button
      angular.element(element[0].querySelector('path#gl-close-btn-path')).css({
        fill: Colours.getHeaderColour()
      });

      // Check how many images are loaded
      var loadInfo = {
        loaded: 0,
        total: scope.images.length
      };
      scope.$on('lightbox.image.load', function () {
        loadInfo.loaded++;
        if (loadInfo.loaded === loadInfo.total) {
          $log.debug('Gallery: All images loaded');
        }
      });

      $timeout(initFlickity);
    }
  };
}];
