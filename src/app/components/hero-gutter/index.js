/**
 * Hero gutter directive
 * Sets hero gutter and background gutter colour
 *
 * @usage: <div gl-gutter-hero>
 */
var heroGutterDirective = ['$window', 'BREAKPOINTS', 'Colours', function ($window, BREAKPOINTS, Colours) {

  function setBackgroundColour(element) {
    var mediaQuery = BREAKPOINTS.TABLET; // 'only screen and (max-width: 667px)';
    if (!$window.matchMedia(mediaQuery).matches) {
      element.css({
        'background-color': Colours.getGutterColour()
      });
      element.parent().css({
        'background-color': Colours.getGutterColour()
      });
    }
  }

  return {
    restrict: 'EA',
    replace: true,
    template: require('./template.html'),
    link: function (scope, element) {
      setBackgroundColour(element);
    }
  };
}];

module.exports = angular
  .module('gl-app.hero-gutter', [])
  .directive('glHeroGutter', heroGutterDirective);
