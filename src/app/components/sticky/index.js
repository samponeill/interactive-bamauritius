var stickyDirective = require('./sticky-directive');

module.exports = angular
  .module('gl-app.sticky', [])
  .directive('glSticky', stickyDirective);