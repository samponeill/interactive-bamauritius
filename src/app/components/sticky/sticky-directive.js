var raf = require('raf');

var getScrollTop = function () {
  if (window.pageYOffset) return window.pageYOffset;
  return document.documentElement.clientHeight ? document.documentElement.scrollTop : document.body.scrollTop;
};

module.exports = ['$timeout', '$log', '$window', 'Tracker', 'BREAKPOINTS', function ($timeout, $log, $window, Tracker, BREAKPOINTS) {
  return {
    restrict: 'EA',
    link: function (scope, element, attrs) {
      var win, lastScrollTop, ticking, elements, isMobile, isLandscape;

      win = angular.element($window);

      function resetElements() {
        angular.forEach(elements, function (value, key, list) {
          // Get header element
          var el = value.wrapper;

          if (el) {
            el.data('sticky', false);
          }

          if (el.content) {
            el.content.css({
              transform: "translate3d(0, 0, 0)",
              position: "absolute"
            });
          }
        });
      }

      function getLastSticky(list) {
        var lastIndex = 0;
        angular.forEach(list, function (value, key) {
          var el = value.wrapper;
          if (el && el.data('sticky')) {
            ++lastIndex;
          }
        });
        return lastIndex;
      }

      /**
       * For event tracking, see if user scrolled to bottom of page
       */
      var isHitBottom = false;
      function checkScrollToBase(x) {
        if (isHitBottom) {
          return;
        }

        var b = document.documentElement.clientHeight - window.innerHeight;

        if (!(b - x)) {
          isHitBottom = true;
          // Send event to GA
          Tracker.trackEvent(
            Tracker.CAT_DESKTOP_PAGES,
            Tracker.ACTION_CHAPTER_VIEW,
            Tracker.LABEL_FINAL_SECTION
          );
        }
      }

      function update() {
        ticking = false;

        checkScrollToBase(lastScrollTop);

        angular.forEach(elements, function (value, key, list) {
          var oy, ty, el, h, scrollY;

          // Get scroll top
          scrollY = lastScrollTop;

          // Get header warpper element
          el = value.wrapper;

          // Get original position of header element
          oy = el.data('originalPosition');

          // Get height of header element
          h = el.data('originalHeight');

          // instagram style push up
          if (key > 0) {

            var offsetY = 0;
            offsetY = oy - (scrollY + h);
            offsetY = Math.min(0, offsetY);

            var prev = elements[ --key ];
            prev.content.css({
              transform: "translate3d(0, " + offsetY + "px, 0)"
            });
          }

          if (!el.data('sticky')) {
            if (oy <= scrollY) {
              el.data('sticky', true);
              value.content.css({
                position: 'fixed',
                'box-shadow': '0px 10px 16px 0px rgba(0,0,0,0.1)'
              });
              scope.$broadcast('enter.section', value.index);
              if (!el.data('viewed')) {
                // Log event for view chapter on desktop
                Tracker.trackEvent(
                  Tracker.CAT_DESKTOP_PAGES,
                  Tracker.ACTION_CHAPTER_VIEW,
                  Tracker.chapterPageLabel(value.index + 1)
                );
                el.data('viewed', true);
              }
            }
          } else {
            if (oy > scrollY) {
              el.data('sticky', false);
              value.content.css({
                position: 'absolute',
                'box-shadow': 'none'
              });
              scope.$broadcast('leave.section', value.index, getLastSticky(list));
            }
          }
        });
      }

      function requestTick() {
        if (!ticking) {
          raf(update);
        }
        ticking = true;
      }

      /**
       * Handle window scroll
       */
      function onScroll(e) {
        lastScrollTop = getScrollTop();
        requestTick();
      }

      var cache = {
        promise: null,
        attempts: 5,
        isLoop: true
      };

      /**
       * Store offset and height for each element
       * TDOO: Refresh this on resize!
       */
      function cachePositions(list) {
        angular.forEach(list, function (value, key, arr) {

          var el, top, height;

          el     = value.wrapper;

          top    = value.background[0].getBoundingClientRect().top;
          height = value.background[0].getBoundingClientRect().height;

          // Offset in case scrolled
          top    = document.querySelector('body').scrollTop + top;

          el.data('originalPosition', top);
          el.data('originalHeight', height);

          el.data('sticky', false);
        });
      }

      function getElements(headingSelector) {
        var els, list;

        els = $window.document.querySelectorAll(headingSelector);
        list = [];

        angular.forEach(els, function (value, key, arr) {

          var el, backgroundEl, contentEl, dotsEl;

          el = angular.element(value);
          backgroundEl = angular.element(value.querySelector('.heading-background'));
          contentEl = angular.element(value.querySelector('.heading-content'));
          dotsEl = angular.element(value.querySelector('.gl-page-dots'));

          list.push({
            wrapper: el,
            content: contentEl,
            background: backgroundEl,
            dots: dotsEl,
            index: key
          });

        });
        return list;

      }
      /**
       * Cache heading positions on ready (all slide directives linked)
       */
      function onSlidesLinked() {

        elements = getElements('.gl-heading.gl-tablet-only');

        function cacheAndListen() {
          // Cache positions
          // Delay for DOM to settle
          startCache();

          win.off('scroll').on('scroll', onScroll);
        }

        // If not mobile, cache positions, add scroll listeners
        var mediaQuery = BREAKPOINTS.TABLET; // 'only screen and (max-width: 667px)';
        if (!$window.matchMedia(mediaQuery).matches) {
          cacheAndListen();
        }
      }

      /**
       * On image load means possible layout refresh
       * So update offsets
       */
      function onImageLoad() {
        if (!$window.matchMedia(BREAKPOINTS.TABLET).matches) {
          startCache();
        }
      }

      function startCache() {
        if (cache.promise) {
          $timeout.cancel(cache.promise);
          cache.promise = null;
        }
        $timeout(function () {
          cachePositions(elements);
          if (cache.isLoop) {
            --cache.attempts;
            cache.isLoop = cache.attempts < 1;
            startCache();
          }
        }, 500);
      }

      function updateMediaBreakPoints() {
        isLandscape = $window.matchMedia(BREAKPOINTS.LANDSCAPE).matches;
        isMobile = $window.matchMedia(BREAKPOINTS.TABLET).matches;
      }

      updateMediaBreakPoints();

      function handleResize() {
        updateMediaBreakPoints();
        resetElements();

        if (!isMobile) {
          startCache();
          onScroll();
        }

        if ($window.matchMedia(BREAKPOINTS.TABLET).matches) {
          win.off('scroll', onScroll);
        } else {
          if (!isMobile) {
            win.off('scroll').on('scroll', onScroll);
          }
        }
      }

      scope.$on('slides.linked', onSlidesLinked);
      scope.$on('slide.image.load', onImageLoad);
      scope.$on('app.resize', handleResize);

      scope.$on('$destroy', function () {
        if (cache.promise) {
          $timeout.cancel(cache.promise);
          cache.promise = null;
          cache.isLoop = false;
        }
        win.off('scroll', onScroll);
      });
    }
  };
}];