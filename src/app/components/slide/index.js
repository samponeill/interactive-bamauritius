/**
 * Slide directive
 *
 * A slide is a scrollable page you can swipe left or right to
 * in the long-form article
 *
 * @usage: <div gl-slide item="{Object}" item-index="{Number}" items="{Array}">
 */
var raf = require('raf');

// Cross browser get scroll top
var getScrollTop = function () {
  if (window.pageYOffset) return window.pageYOffset;
  return document.documentElement.clientHeight ? document.documentElement.scrollTop : document.body.scrollTop;
};

var slideDirective = ['$window', 'Tracker', 'Colours', function ($window, Tracker, Colours) {

  function scrollTracker(el, index) {

    var lastScrollTop, ticking, rawEl, pageIndex;

    rawEl = el[0];
    pageIndex = index + 1;

    function update() {
      var base = rawEl.scrollHeight - rawEl.clientHeight;

      if (!(base - lastScrollTop)) {
        // hit bottom of page
        Tracker.trackEvent(
          Tracker.CAT_MOBILE_PAGES,
          Tracker.ACTION_CHAPTER_VIEW,
          Tracker.chapterPageScrolledLabel(pageIndex)
        );
        el.off('scroll');
        return;
      }

      ticking = false;
    }

    /**
     * Request animation frame wrapper
     */
    function requestTick() {
      if (!ticking) {
        raf(update);
      }
      ticking = true;
    }

    /**
     * Handle window scroll
     */
    function onScroll(e) {
      lastScrollTop = rawEl.scrollTop;
      requestTick();
    }

    // Listen for scroll event
    el.off('scroll').on('scroll', onScroll);

    return function () {
      el.off('scroll');
    };
  }

  return {
    scope: {
      item: '=',
      itemIndex: '=',
      items: '='
    },
    restrict: 'EA',
    replace: true,
    template: require('./template.html'),
    link: function (scope, element) {
      // Set background colour
      element.attr('style', 'background-color:' + Colours.getBackgroundColour());
      // Set slide heading colour
      element.find('h3').eq(0).css({
        color: Colours.getHeaderColour(),
        background: Colours.getBackgroundColour()
      });
      // Set button label colour
      element.find('span').css({
        color: Colours.getHeaderColour()
      });
      // Emit link event
      scope.$emit('slide.linked');
      // Define handler for next click
      scope.onClickNext = function () {
        // Track next click event
        Tracker.trackEvent(
          Tracker.CAT_MOBILE_PAGES,
          Tracker.ACTION_CHAPTER_VIEW,
          Tracker.LABEL_NEXT_CLICK
        );
        scope.$emit('slide.next');
      };

      var destroyScrollTracker = scrollTracker(
        angular.element(element[0].querySelector('.slide-scrollable-wrapper')),
        scope.itemIndex
      );

      scope.$on('$destroy', function () {
        destroyScrollTracker();
      });
    }
  };
}];

module.exports = angular
  .module('gl-app.slide', [])
  .directive('glSlide', slideDirective);
