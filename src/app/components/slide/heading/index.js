require("match-media");
/**
 * Heading directive
 * @usage: <div gl-heading title="{String}" icon="String">
 */
module.exports = angular
  .module('gl-app.slide.heading', [])
  .directive('glHeading', ['$timeout', '$window', 'BREAKPOINTS', 'Colours', function ($timeout, $window, BREAKPOINTS, Colours) {
    return {
      scope: {
        title: '=',
        icon: '=',
        items: '=',
        selectedIndex: '='
      },
      restrict: 'EA',
      replace: true,
      template: require('./template.html'),
      link: function (scope, element, attrs) {

        function updateHeader() {
          // Set text colour
          element.css({
            color: Colours.getHeaderColour()
          });

          var title = angular.element(element[0].querySelector('h2'));
          $timeout(function () {
            // Get content height and set height to container
            var h = title[0].offsetHeight + 20;
            element.css({
              height: h
            });
          });

          element.css({
            "background-color": Colours.getBackgroundColour()
          });

          // Get content
          var content = angular.element(element[0].querySelector('.heading-content'));
          content.css({
            "background-color": Colours.getBackgroundColour()
          });
        }

        updateHeader();

        // Listen for resize events to update image source
        scope.$on('app.resize', function () {
          updateHeader();
        });
      }
    };
  }
]);