require("match-media");

/**
 * Photo directive
 * @usage: <div gl-photo url="{String}">
 */
var photoDirective = ['$log', '$sce', 'Tracker', 'Colours', 'BREAKPOINTS', function ($log, $sce, Tracker, Colours, BREAKPOINTS) {

  function handleImageError() {
    $log.debug('Error: Image not loaded');
    if (this.$emit) this.$emit('slide.image.error');
  }

  function handleImageLoad() {
    if (this.$emit) this.$emit('slide.image.load');
  }

  return {
    scope: {
      columnId: '=',
      imageUrl: '=',
      imageMobileUrl: '=',
      caption: '='
    },
    restrict: 'EA',
    replace: true,
    template: require('./template.html'),
    controller: ['$scope', '$window', function ($scope, $window) {

      // Determine which image to use by media
      function updateImageSource() {
        var mediaQuery = BREAKPOINTS.TABLET; // 'only screen and (max-width: 667px)';
        if ($window.matchMedia(mediaQuery).matches) {
          // Use mobile image
          $scope.imageSrc = $window.matchMedia(BREAKPOINTS.LANDSCAPE).matches ? $scope.imageUrl : ($scope.imageMobileUrl || $scope.imageUrl);
        } else {
          $scope.imageSrc = $scope.imageUrl;
        }
      }

      // Emit photo event upwards on click
      $scope.onPhotoClick = function () {
        // Track open lightbox event
        Tracker.trackEvent(
          Tracker.CAT_MOBILE_PAGES,
          Tracker.ACTION_GALLERY_VIEW,
          Tracker.LABEL_GALLERY_OPEN
        );
        $scope.$emit('photo.click', $scope.columnId);
      };

      // Listen for resize events to update image source
      $scope.$on('app.resize', function () {
        updateImageSource();
      });

      // Set on first load
      updateImageSource();
    }],
    link: function (scope, element, attrs) {

      var src, img = element.find('img');

      img.on('load', function () {
        handleImageLoad.call(scope);
      });
      img.on('error', function () {
        handleImageError.call(scope);
      });

      function setImageSource(newSrc) {
        src = newSrc;
        img.attr('src', src);
      }

      scope.$watch('imageSrc', function (newValue, oldValue) {
        if (newValue && newValue !== src) {
          setImageSource(newValue);
        }
      });

      // Set icon backgroudn colour
      element.find('circle').css({
        fill: 'rgba(0,0,0,0.3)' // Colours.getHeaderColour()
      });
      // Set icon camera colour
      element.find('path').css({
        fill: 'white' // Colours.getBackgroundColour()
      });
    }
  };
}];

module.exports = angular
  .module('gl-app.slide.photo', [])
  .directive('glPhoto', photoDirective);