/**
 * Text directive
 * @usage: <div gl-text content="{String}">
 */
module.exports = angular
  .module('gl-app.slide.text', [])
  .directive('glText', ['$sce', 'Colours', function ($sce, Colours) {
    return {
      scope: {
        content: '='
      },
      restrict: 'EA',
      replace: true,
      template: require('./template.html'),
      controller: ['$scope', function ($scope) {

        // Replace newline with <br> tag
        var str = $scope.content;
        str = str.replace(/(?:\r\n|\r|\n)/g, '<br />');

        $scope.safeContent = $sce.trustAsHtml(str);
      }],
      link: function (scope, element, attrs) {
        // Set text colour from service
        element.css({
          color: Colours.getBodyColour()
        });
      }
    };
  }
]);