module.exports = angular
  .module('gl-app.slide.video', [])
  .directive('glVideo', ['$log', '$sce', function ($log, $sce) {
    return {
      scope: {
        videoInfo: '='
      },
      replace: true,
      restrict: 'EA',
      template: '<div class="gl-video"><div class="gl-video-wrapper"><iframe frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div></div>',
      link: function (scope, element, attrs) {

        function createEmbedElement(embed) {
          var el = document.createElement('div');
          el.innerHTML = $sce.trustAsHtml(embed);
          return el;
        }

        function getVideoEmbed() {
          var el = createEmbedElement(scope.videoInfo);
          var iframes = el.getElementsByTagName('iframe');
          if (iframes.length) return iframes[0];
          return null;
        }

        var iframe = getVideoEmbed();

        if (iframe) {
          var src = iframe.getAttribute('src');
          var w = iframe.getAttribute('width') ? parseInt(iframe.getAttribute('width'), 10) : 500;
          var h = iframe.getAttribute('height') ? parseInt(iframe.getAttribute('height'), 10) : 281;
          var ratio = (h / w * 100) + '%';

          element.find('iframe').attr('src', src);
          var wrapper = angular.element(element[0].querySelector('.gl-video-wrapper'));
          wrapper.css({
            "padding-bottom": ratio + "%"
          });
        } else {
          $log.debug('Video error: No video embed code found');
        }
      }
    };
  }]);
