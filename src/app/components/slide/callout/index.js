/**
 * Callout directive
 * @usage: <div gl-callout content="{String}">
 */
var calloutDirective = ['$sce', 'Colours', function ($sce, Colours) {
  return {
    scope: {
      content: '='
    },
    restrict: 'EA',
    replace: true,
    template: require('./template.html'),
    controller: ['$scope', function ($scope) {
      $scope.safeContent = $sce.trustAsHtml($scope.content);
    }],
    link: function (scope, element, attrs) {
      element.find('blockquote').css({
        color: Colours.getHeaderColour()
      });
    }
  };
}];

module.exports = angular
  .module('gl-app.slide.callout', [])
  .directive('glCallout', calloutDirective);