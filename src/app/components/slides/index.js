var slidesController = require('./slides-controller');
var slidesService = require('./slides-service');
var slidesDirective = require('./slides-directive');

module.exports = angular
  .module('gl-app.slides', [])
  .controller('SlidesController', slidesController)
  .factory('SlidesService', slidesService)
  .directive('glSlides', slidesDirective);

