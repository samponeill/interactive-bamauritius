/**
 * Slides Data Service to parse Google Sheet data
 */
module.exports = ['$log', 'SHEETS', 'Colours', function ($log, SHEETS, Colours) {

  var data;

  function parseData(res) {
    data = res[0];
    return data;
  }

  function setColours() {
    var isColourSet = false;

    if (data && data[SHEETS.STYLES]) {

      // Set the colours from the Styles
      var item = data[SHEETS.STYLES].all()[0];

      if (item) {
        if (item.header_text_colour) {
          Colours.setHeaderColour(item.header_text_colour);
        }
        if (item.body_text_colour) {
          Colours.setBodyColour(item.body_text_colour);
        }
        if (item.background_colour) {
          Colours.setBackgroundColour(item.background_colour);
        }
        if (item.gutter_background_colour) {
          Colours.setGutterColour(item.gutter_background_colour);
        }
        if (item.gutter_hero_background_colour) {
          Colours.setGutterHeroColour(item.gutter_hero_background_colour);
        }
        isColourSet = true;
      }

    } else {
      $log.error(SHEETS.STYLES + ' data does not exist');
    }

    return isColourSet;
  }

  /**
   * Create list for template
   */
  function getList() {

    var  list = [];

    /**
     * Populate list for template
     * from 'Content' in the Google Sheet data
     */
    if (data && data[SHEETS.CONTENT]) {
      list = data[SHEETS.CONTENT].all();
    } else {
      $log.error(SHEETS.CONTENT + ' data does not exist');
    }

    return list;
  }

  /**
   * Create list of content blocks for this slide
   * eg. [ { image_1:"urltosomething", caption_1:"my caption", text_1... }, { image_2 caption_2 ... } ... ]
   */
  function getContentBlocks(slide) {

    // List blocks, list found block ids
    var blocks = [], foundIds = [];

    function getBlockId(k) {
      return k.match(/(_)(\d)+(_?)/);
    }

    function getBlock(id) {
      var ret;
      blocks.forEach(function (value) {
        if (value.id === id) ret = value;
      });
      return ret;
    }

    function idExists(id, idList) {
      return idList.indexOf(id) !== -1;
    }

    function stripKey(key, id) {
      var index = key.indexOf('_' + id);
      var len = id.length + 1;

      return key.substr(0, index) + key.substr(index + len);
    }

    Object.keys(slide).forEach(function (value) {

      var match, id, block, newKey;

      match = getBlockId(value);

      if (!match) return;

      id = parseInt(match[2], 10);
      newKey = stripKey(value, String(id));

      if (idExists(id, foundIds)) {
        // old block
        block = getBlock(id);
        block[newKey] = slide[value];
      } else {

        foundIds.push(id);

        // new block
        block = { id: id };
        block[newKey] = slide[value];
        blocks.push(block);
      }
    });

    // Append to original data source
    slide.blocks = blocks;
  }

  /**
   * Add content blocks to all slides
   */
  function allContentBlocks() {
    getList().forEach(getContentBlocks);
  }

  /**
   * Get index slide data
   */
  function getSlideIndex() {
    var slideIndex;

    if (data && data[SHEETS.INDEX]) {
      var indexData = data[SHEETS.INDEX].all();
      if (indexData.length) {
        slideIndex = indexData[0];
      } else {
        $log.error(SHEETS.INDEX + ' is empty');
      }
    } else {
      $log.error(SHEETS.INDEX + ' data does not exist');
    }

    return slideIndex;
  }

  /**
   * Get end related content slide data
   */
  function getRelatedContent() {
    var relatedData;

    if (data && data[SHEETS.RELATED]) {
      relatedData = data[SHEETS.RELATED].all();
    } else {
      $log.error(SHEETS.RELATED + ' data does not exist');
    }

    return relatedData;
  }

  /**
   * Get end credit slide data
   */
  function getCredits() {
    var credits;

    if (data && data[SHEETS.CREDITS]) {
      credits = data[SHEETS.CREDITS].all();
    } else {
      $log.error(SHEETS.CREDITS + ' data does not exist');
    }

    return credits;
  }

  var factory = {
    parseData: parseData,
    getList: getList,
    getSlideIndex: getSlideIndex,
    getRelatedContent: getRelatedContent,
    getCredits: getCredits,
    allContentBlocks: allContentBlocks,
    setColours: setColours
  };

  return factory;
}];