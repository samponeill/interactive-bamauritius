module.exports = ['$scope', 'data', 'SlidesService', 'Colours', function ($scope, data, SlidesService, Colours) {

  $scope.data = SlidesService.parseData(data);

  SlidesService.allContentBlocks();

  SlidesService.setColours();

  $scope.list = SlidesService.getList();

  $scope.slideIndex = SlidesService.getSlideIndex();

  $scope.relatedData = SlidesService.getRelatedContent();

  $scope.creditsData = SlidesService.getCredits();

}];