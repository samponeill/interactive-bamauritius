/**
 * Slides directive
 *
 * @usage: <div gl-slides>
 */

require('match-media');
var Flickity = require('flickity');

/**
 * Override Flickity scroll drag threshold
 * To allow easier vertical scroll
 */
Flickity.prototype.hasDragStarted = function (moveVector) {
  // start dragging after pointer has moved 3 pixels in either direction
  return !this.isTouchScrolling && Math.abs(moveVector.x) > 20;
};

module.exports = ['$log', '$window', '$timeout', 'Tracker', 'Colours', 'BREAKPOINTS', function ($log, $window, $timeout, Tracker, Colours, BREAKPOINTS) {

  /**
   * Flickity instance
   */
  var flick;

  return {
    restrict: 'EA',
    replace: true,
    template: require('./template.html'),
    controller: ['$scope', function ($scope) {

      /**
       * Listen for slide link events
       * Total slides is content pages + index page + end page
       */
      var linked = 0, totalSlides = $scope.list.length + 2;
      $scope.$on('slide.linked', function (e) {
        if (++linked === totalSlides) {
          $scope.$emit('slides.linked');
        }
      });

    }],
    link: function (scope, element, attrs) {

      /**
       * Handle cell select
       * Track event
       */
      function onCellSelect() {
        Tracker.trackEvent(
          Tracker.CAT_MOBILE_PAGES,
          Tracker.ACTION_CHAPTER_VIEW,
          Tracker.chapterPageLabel(flick.selectedIndex)
        );

        if (flick.selectedIndex === (flick.cells.length - 1)) {
          Tracker.trackEvent(
            Tracker.CAT_MOBILE_PAGES,
            Tracker.ACTION_CHAPTER_VIEW,
            Tracker.LABEL_FINAL_SECTION
          );
        }
      }
      /**
       * Init Flickity unless already exists
       */
      function initFlickity() {
        if (!flick) {
          flick = new Flickity('.scroll-wrapper', {
            cellSelector: 'section.slide-section',
            prevNextButtons: false,
            setGallerySize: false,
            resize: true,
            freeScroll: false,
            pageDots: true,
            selectedAttraction: 0.15,
            friction: 0.9
          });
          flick.on('cellSelect', onCellSelect);
        }
      }

      /**
       * Destroy Flickity if exists
       */
      function destroyFlickity() {
        if (flick) {
          flick.off('cellSelect', onCellSelect);
          flick.destroy();
          flick = null;
        }
      }

      /**
       * Init / Destroy Flickity based on media query
       */
      function updateFlickity() {
        var mediaQuery = BREAKPOINTS.TABLET;
        if ($window.matchMedia(mediaQuery).matches) {
          // Now check if it is landscape
          if ($window.matchMedia(BREAKPOINTS.LANDSCAPE).matches) {
            destroyFlickity();
          } else {
            initFlickity();
          }
        } else {
          destroyFlickity();
        }
      }

      /**
       * Listen for photo click
       * Should trigger lightbox, but first off, disable Flickity
       * and all scrolling
       * @param {Event}
       * @param {String} Photo id
       */
      scope.$on('photo.click', function (e, photoId) {
        // Trigger lightbox to open
        scope.$broadcast('lightbox.open', photoId);
      });

      /**
       * Listen for event signalling all slides linked to DOM
       * Initiate Flickity for horizontal scroll interaction
       * For tablet and above, skip Flickity init
       */
      var isLinked = false;
      scope.$on('slides.linked', function () {
        isLinked = true;
        updateFlickity();
      });

      /**
       * Respond to slide.next event by advancing flickity cell
       */
      scope.$on('slide.next', function () {
        if (flick) {
          flick.next();
        }
      });

      /**
       * Respond to browser resizing
       * Destroy / Init Flickity for tablet / mobile
       */
      var resizer = {
        promise: null,
        n: 0,
        delay: 200
      };

      function handleResize() {
        resizer.n++;
        updateFlickity();
        scope.$broadcast('app.resize');
      }

      /**
       * Attach handler to window resize event
       */
      angular.element($window).on('resize', function () {
        if (resizer.promise) {
          $timeout.cancel(resizer.promise);
        }
        resizer.promise = $timeout(handleResize, resizer.delay);
      });

      var orient = {
        promise: null
      };

      function handleOrientationChange() {
        $log.debug('handleOrientationChange');
        $log.debug($window.orientation);

        updateFlickity();

        scope.$broadcast('app.resize');
      }

      /**
       * Attach handler to window orientation event
       */
      angular.element($window).on('orientationchange', function () {
        if (orient.promise) {
          $timeout.cancel(orient.promise);
        }
        orient.promise = $timeout(handleOrientationChange);
      });

      /**
       * Destroy Flickity instance on directive removal
       */
      scope.$on('$destroy', function () {
        if (flick) {
          flick.destroy();
          flick = null;
        }
        angular.element($window).off('resize');
        angular.element($window).off('orientationchange');
      });
    }
  };
}];