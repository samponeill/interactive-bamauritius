var creditDirective = ['Colours', function (Colours) {
  return {
    scope: {
      content: '='
    },
    replace: true,
    template: '<div class="gl-credit"><div class="credit-title">{{ content.title }} &mdash;&nbsp;</div><div class="credit-name">{{ content.name }}</div></div>',
    link: function (scope, element, attrs) {
      angular.element(element[0].querySelector('.credit-title')).css({
        color: Colours.getHeaderColour()
      });
      angular.element(element[0].querySelector('.credit-name')).css({
        color: Colours.getBodyColour()
      });
    }
  };
}];

module.exports = angular
  .module('gl-app.end-slide.credit', [])
  .directive('glCredit', creditDirective);