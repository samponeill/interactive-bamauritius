/**
 * End Slide directive
 * Related content and credits
 *
 * @usage: <div gl-end-slide item='{Object}'>
 */
var endSlideDirective = ['$log', 'Tracker', 'Colours', function ($log, Tracker, Colours) {
  return {
    scope: {
      related: '=',
      credits: '=',
      index: '='
    },
    restrict: 'EA',
    replace: true,
    template: require('./template.html'),
    controller: ['$scope', function ($scope) {
      $scope.onClientClick = function (e) {
        Tracker.trackEvent(
          Tracker.CAT_LINK,
          Tracker.ACTION_OUTBOUND_LINK,
          Tracker.LABEL_CLICKED_LOGO
        );
      };
    }],
    link: function (scope, element) {
      element.find('h3').css({
        color: Colours.getHeaderColour(),
        'border-top': '1px solid ' + Colours.getHeaderColour()
      });
      element.find('h3').eq(0).css({
        'border-top': 'none'
      });
      element.find('h4').css({
        color: Colours.getHeaderColour(),
        'border-top': '1px solid ' + Colours.getHeaderColour()
      });
      // Emit link event
      scope.$emit('slide.linked');
    }
  };
}];

module.exports = angular
  .module('gl-app.end-slide', [])
  .directive('glEndSlide', endSlideDirective);
