var relatedDirective = ['Colours', function (Colours) {
  return {
    scope: {
      item: '='
    },
    replace: true,
    template: require('./template.html'),
    link: function (scope, element) {
      angular.element(element[0].querySelector('.related-description')).css({
        color: Colours.getBodyColour()
      });
    }
  };
}];

module.exports = angular
  .module('gl-app.end-slide.related', [])
  .directive('glRelated', relatedDirective);