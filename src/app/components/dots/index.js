var dotsDirective = require('./dots-directive');

module.exports = angular
  .module('gl-app.dots', [])
  .directive('glDots', dotsDirective);
