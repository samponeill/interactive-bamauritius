/**
 * Desktop page dots directive
 *
 * @usage: <div gl-nav-dots>
 */
module.exports = ['$timeout', '$log', '$window', 'BREAKPOINTS', function ($timeout, $log, $window, BREAKPOINTS) {

  function updateIndex(isEnter, el) {
    return function () {
      var args = [].slice.call(arguments, 0);

      if (args.length < 2) {
        $log.error('Error: Not enough arguments');
        return;
      }
      if (isNaN(args[1])) {
        $log.error('Error: Index arg must be a number');
        return;
      }
      if (!isEnter && args.length < 3) {
        $log.error('Error: Leave: Not enough arguments');
        return;
      }

      var index = args[1];
      var list = [].slice.call(el[0].querySelectorAll('li.dot'), 0);

      angular.forEach(list, function (value) {
        value.classList.remove('is-selected');
      });

      var listItem = list[index];

      if (!listItem) {
        $log.error('Error: List item is null');
        return;
      }

      if (isEnter) {
        if (el.hasClass('is-hidden')) {
          el.removeClass('is-hidden');
        }
        listItem.classList.add('is-selected');
      } else {
        var lastListItem = list[args[2] - 1];
        if (lastListItem) {
          lastListItem.classList.add('is-selected');
        } else {
          el.addClass('is-hidden');
        }
      }
    };
  }

  return {
    scope: {
      items: '='
    },
    restrict: 'EA',
    replace: true,
    template: require('./template.html'),
    link: function (scope, element) {

      var mediaQuery = BREAKPOINTS.TABLET; // 'only screen and (max-width: 667px)';
      if ($window.matchMedia(mediaQuery).matches) return;

      var enterOffFn, leaveOffFn, timer;

      function addListeners() {
        enterOffFn = scope.$on('enter.section', updateIndex(true, element));
        leaveOffFn = scope.$on('leave.section', updateIndex(false, element));
      }

      addListeners();

      scope.onItemClick = function (index) {

        var fn = updateIndex(true, element);
        fn.call(scope, null, index);

        enterOffFn();
        leaveOffFn();

        if (timer) {
          $timeout.cancel(timer);
          timer = null;
        }
        timer = $timeout(addListeners, 10);

        return true;
      };

      scope.$on('$destroy', function () {
        if (timer) {
          $timeout.cancel(timer);
          timer = null;
        }
      });
    }
  };
}];