/**
 * Index Slide directive
 *
 * @usage: <div gl-index-slide item="{Object}">
 */
var indexSlideDirective = ['$window', 'Tracker', 'BREAKPOINTS', 'Colours', function ($window, Tracker, BREAKPOINTS, Colours) {

  function getUrl(src) {
    return  'url(' + src + ')';
  }

  function getBackgroundImage(item) {
    if (!item) {
      return 'none';
    }
    var mediaQuery = BREAKPOINTS.TABLET; // 'only screen and (max-width: 667px)';
    if ($window.matchMedia(mediaQuery).matches) {
      // Use mobile image
      return getUrl(item.background_mobile_image || item.background_image);
    } else {
      return getUrl(item.background_image);
    }
    return 'none';
  }

  return {
    scope: {
      item: '='
    },
    restrict: 'EA',
    replace: true,
    template: require('./template.html'),
    controller: ['$scope', function ($scope) {
      $scope.onClientClick = function (e) {
        Tracker.trackEvent(
          Tracker.CAT_LINK,
          Tracker.ACTION_OUTBOUND_LINK,
          Tracker.LABEL_CLICKED_LOGO
        );
      };
    }],
    link: function (scope, element) {
      var backgroundImageEl = angular.element(element[0].querySelector('.index-background-image'));
      // Set background image and colour
      backgroundImageEl.css({
        'background-color': Colours.getBackgroundColour(),
        'background-image': getBackgroundImage(scope.item)
      });
      //
      var contentWrapperEl = angular.element(element[0].querySelector('.slide-content-wrapper'));
      contentWrapperEl.css({
        'background-color': Colours.getBackgroundColour()
      });
      element.css({
        'background-color': Colours.getGutterColour()
      });
      angular.element(element[0].querySelector('.slide-index-description')).css({
        color: Colours.getBodyColour()
      });
      angular.element(element[0].querySelector('h4.slide-index-client-title')).css({
        color: Colours.getHeaderColour()
      });

      // Set slide heading colour
      element.find('h3').eq(0).attr('style', 'color:' + Colours.getHeaderColour());

      // Set button label colour
      element.find('span').css({
        color: Colours.getHeaderColour()
      });
      // Emit link event
      scope.$emit('slide.linked');
      // Define handler for next click
      scope.onClickNext = function () {
        scope.$emit('slide.next');
      };
    }
  };
}];

module.exports = angular
  .module('gl-app.index-slide', [])
  .directive('glIndexSlide', indexSlideDirective);
