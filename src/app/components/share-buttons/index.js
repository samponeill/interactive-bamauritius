var shareDirective = require('./share-directive');

module.exports = angular
  .module('gl-app.sharer', [])
  .directive('glShareButtons', shareDirective);

