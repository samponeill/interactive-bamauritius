module.exports = ['Social', function (Social) {
  return {
    restrict: 'EA',
    replace: true,
    template: require('./template.html'),
    controller: ['$scope', function ($scope) {
      $scope.shareFacebook = Social.openFacebook;
      $scope.shareTwitter = Social.openTwitter;
    }]
  };
}];