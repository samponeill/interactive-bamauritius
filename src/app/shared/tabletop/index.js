/**
 * Tabletop Provider for states
 */
var Tabletop = require('tabletop');

function TabletopProvider() {
  var tabletopResponse;

  var tabletopOptions = {
    callback: function (data, Tabletop) {
      tabletopResponse.resolve([data, Tabletop]);
    }
  };

  // Public API for configuration
  this.setTabletopOptions = function (opts) {
    tabletopOptions = angular.extend(tabletopOptions, opts);
  };

  // Method for instantiating
  this.$get = ['$q', function ($q) {
    tabletopResponse = $q.defer();
    Tabletop.init(tabletopOptions);
    return tabletopResponse.promise;
  }];
}

module.exports = angular
  .module('gl-app.shared.tabletop', [])
  .provider('Tabletop', TabletopProvider);
