/**
 * Store colours in this service
 */
function ColoursService() {

  var colours = {
    background: '#3d0',
    header: '#fc0',
    body: '#0fc',
    gutter: '#000',
    gutter_hero: '#fcf'
  };

  function setGutterColour(colour) {
    colours.gutter = colour;
  }

  function setGutterHeroColour(colour) {
    colours.gutter_hero = colour;
  }

  function getGutterColour() {
    return colours.gutter;
  }

  function getGutterHeroColour() {
    return colours.gutter_hero;
  }

  function setHeaderColour(colour) {
    colours.header = colour;
  }

  function getHeaderColour() {
    return colours.header;
  }

  function setBackgroundColour(colour) {
    colours.background = colour;
  }

  function getBackgroundColour() {
    return colours.background;
  }

  function getBodyColour() {
    return colours.body;
  }

  function setBodyColour(colour) {
    colours.body = colour;
  }

  var factory = {
    setHeaderColour: setHeaderColour,
    setBodyColour: setBodyColour,
    setBackgroundColour: setBackgroundColour,
    setGutterColour: setGutterColour,
    setGutterHeroColour: setGutterHeroColour,
    getBodyColour: getBodyColour,
    getHeaderColour: getHeaderColour,
    getBackgroundColour: getBackgroundColour,
    getGutterColour: getGutterColour,
    getGutterHeroColour: getGutterHeroColour
  };

  return factory;
}

module.exports = angular
  .module('gl-app.shared.colours', [])
  .factory('Colours', ColoursService);
