/**
 * Social Service for opening Twitter / Facebook dialogs
 */
var SocialService = ['Tracker', '$window', function (Tracker, $window) {

  var FACEBOOK_SHARE_BASE_URL = 'https://www.facebook.com/sharer/sharer.php';

  var TWITTER_SHARE_BASE_URL = 'https://twitter.com/intent/tweet';

  /**
   * Open window of certain size
   * @param {String} Url for dialog
   * @param {Number} Width of window
   * @param {Number} Height of window
   */
  function openDialog(url, width, height, x, y) {

    width = width || 520;
    height = height || 300;

    x = x || 100;
    y = y || 100;

    $window.open(url, 'sharer', 'top=' + y + ',left=' + x + ',toolbar=0,status=0,width=' + width + ',height=' + height);
  }

  function openFacebook() {
    var shareUrl = FACEBOOK_SHARE_BASE_URL + '?u=' + escape('http://google.com');

    Tracker.trackEvent(
      Tracker.CAT_LINK,
      Tracker.ACTION_SHARE,
      Tracker.LABEL_SHARE_FACEBOOK
    );

    openDialog(shareUrl, 560, 460);
  }

  function openTwitter() {
    var message = "Twitter test";
    var shareUrl = TWITTER_SHARE_BASE_URL + '?text=' + message + '&url=' + escape($window.location.href);

    Tracker.trackEvent(
      Tracker.CAT_LINK,
      Tracker.ACTION_SHARE,
      Tracker.LABEL_SHARE_TWITTER
    );

    openDialog(shareUrl);
  }

  var factory = {
    openFacebook: openFacebook,
    openTwitter: openTwitter
  };

  return factory;
}];

module.exports = angular
  .module('gl-app.shared.social', [])
  .factory('Social', SocialService);
