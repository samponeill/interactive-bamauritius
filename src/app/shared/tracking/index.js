/**
 * Google Analytics Service for tracking
 */
var TrackerService = ['$log', '$window', function ($log, $window) {

  var UA = 'UA-72576373-1';

  var CAT_DESKTOP_PAGES = 'desktop_pages';
  var CAT_MOBILE_PAGES = 'mobile_pages';
  var CAT_LINK = 'link';

  var ACTION_CHAPTER_VIEW = 'chapter_view';
  var ACTION_GALLERY_VIEW = 'gallery_view';
  var ACTION_SHARE = 'share';
  var ACTION_OUTBOUND_LINK = 'outbound_link';

  var LABEL_CHAPTER_VIEW = 'chapter%_view';
  var LABEL_CHAPTER_SCROLLED = 'chapter%_scrolled';
  var LABEL_FINAL_SECTION = 'reached_final_section';
  var LABEL_NEXT_CLICK = 'next_clicked';
  var LABEL_GALLERY_OPEN = 'opened_gallery';
  var LABEL_SHARE_FACEBOOK = 'share_facebook';
  var LABEL_SHARE_TWITTER = 'share_twitter';
  var LABEL_CLICKED_LOGO = 'clicked_logo';

  /**
   * Send to Google Analytics if available
   * @param {Object} Content to send with event
   */
  function send(params) {
    try {
      $window.ga('send', params);
    } catch (e) {
      $log.error('No ga method available', e, params);
    }
  }

  /**
   * Pass message to Google Analytics tracking
   * @param {String} category Category name
   * @param {String} action Action name
   * @param {String} label Label name
   */
  function trackEvent(category, action, label) {
    $log.debug('Track page event', category, action, label);

    send({
      hitType: 'event',
      eventCategory: category,
      eventAction: action,
      eventLabel: label
    });
  }

  function chapterPageLabel(pageIndex) {
    var str = LABEL_CHAPTER_VIEW.replace('%', pageIndex);
    return str;
  }

  function chapterPageScrolledLabel(pageIndex) {
    var str = LABEL_CHAPTER_SCROLLED.replace('%', pageIndex);
    return str;
  }

  var factory = {

    CAT_DESKTOP_PAGES: CAT_DESKTOP_PAGES,
    CAT_MOBILE_PAGES: CAT_MOBILE_PAGES,
    CAT_LINK: CAT_LINK,

    ACTION_CHAPTER_VIEW: ACTION_CHAPTER_VIEW,
    ACTION_GALLERY_VIEW: ACTION_GALLERY_VIEW,
    ACTION_SHARE: ACTION_SHARE,
    ACTION_OUTBOUND_LINK: ACTION_OUTBOUND_LINK,

    LABEL_CHAPTER_VIEW: LABEL_CHAPTER_VIEW,
    LABEL_FINAL_SECTION: LABEL_FINAL_SECTION,
    LABEL_NEXT_CLICK: LABEL_NEXT_CLICK,
    LABEL_GALLERY_OPEN: LABEL_GALLERY_OPEN,
    LABEL_SHARE_FACEBOOK: LABEL_SHARE_FACEBOOK,
    LABEL_SHARE_TWITTER: LABEL_SHARE_TWITTER,
    LABEL_CLICKED_LOGO: LABEL_CLICKED_LOGO,

    trackEvent: trackEvent,
    chapterPageLabel: chapterPageLabel,
    chapterPageScrolledLabel: chapterPageScrolledLabel
  };

  return factory;
}];

module.exports = angular
  .module('gl-app.tracker', [])
  .factory('Tracker', TrackerService);
