module.exports = ['$compileProvider', '$logProvider', '$locationProvider', '$urlRouterProvider', '$stateProvider', 'TabletopProvider', function ($compileProvider, $logProvider, $locationProvider, $urlRouterProvider, $stateProvider, TabletopProvider) {

  var isDebug = true;

  $compileProvider.debugInfoEnabled(isDebug);
  $logProvider.debugEnabled(isDebug);

  $urlRouterProvider.otherwise('/');

  $locationProvider.html5Mode({
    enabled: true,
    requireBase: true
  });

  // Set Google Sheets key for Tabletop.js
  TabletopProvider.setTabletopOptions({
    key: 'https://docs.google.com/spreadsheets/d/10Bmwet9C7dIgB822lX7IUtFY3En4swv0Nw50I_q4CKY/pubhtml'
  });

  $stateProvider

    /**
     * Index state
     * List of slides generated from Google Sheets > Tabletop.js
     */
    .state('slides', {
      url: '/',
      resolve: {
        data: ['Tabletop', function (Tabletop) {
          return Tabletop.then(function (res) {
            return res;
          });
        }]
      },
      controller: 'SlidesController',
      template: '<div><div gl-hero-gutter class="gl-tablet-only"></div><div gl-slides gl-sticky></div><div gl-lightbox></div></div>'
    });
}];
