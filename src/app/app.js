var angular = require('angular');

angular
  .module('gl-app', [
    require('./shared/tracking/').name,
    require('./shared/tabletop').name,
    require('./shared/colours').name,
    require('./shared/social').name,
    require('angular-ui-router'),
    require('./components/dots').name,
    require('./components/hero-gutter').name,
    require('./components/share-buttons').name,
    require('./components/slide').name,
    require('./components/index-slide').name,
    require('./components/end-slide').name,
    require('./components/end-slide/credit').name,
    require('./components/end-slide/related').name,
    require('./components/slide/photo').name,
    require('./components/slide/text').name,
    require('./components/slide/heading').name,
    require('./components/slide/callout').name,
    require('./components/slide/video').name,
    require('./components/slides').name,
    require('./components/lightbox').name,
    require('./components/lightbox/item').name,
    require('./components/sticky').name
  ])
  .constant('BREAKPOINTS', {
    TABLET: 'only screen and (max-width: 667px)',
    LANDSCAPE: 'only screen and (orientation: landscape)'
  })
  .constant('SHEETS', {
    INDEX: 'Index',
    CONTENT: 'Content',
    RELATED: 'Related',
    CREDITS: 'Credits',
    STYLES: 'Styles'
  })
  .controller('AppController', function () {})
  .config(require('./app.routes'));
